# 欢迎使用Block eechen浏览器扩展

> * **我的OSC我做主，屏蔽eechen拒绝复制粘贴党**

## 功能列表
- [x] 屏蔽博客中的评论
- [x] 屏蔽新闻中的评论
- [x] 屏蔽问答中的评论
- [x] 屏蔽翻译中的评论
- [x] 屏蔽代码分享中的评论
- [x] 屏蔽动弹页中的评论
- [x] 屏蔽博客中他人引用的评论
- [x] 屏蔽新闻中他人引用的评论
- [x] 屏蔽问答中子评论及回复
- [x] 屏蔽翻译中他人引用的评论
- [x] 屏蔽代码分享中他人引用的评论
- [ ] 自定义屏蔽列表

## 安装使用

#### 直接下载crx安装

 1. 点[附件](http://git.oschina.net/shentu-jiazhen/Block-eechen/attach_files)下载最新的crx
 2. 拖拽crx文件到chrome扩展程序页面

#### 打包安装

 1. 点[Zip](http://git.oschina.net/shentu-jiazhen/Block-eechen/repository/archive/master)下载最新源码
 2. 解压源码
 3. 在chrome扩展程序页面勾选开发者模式
 4. 点击‘加载已解压的扩展程序’ 选择源码解压目录

### 安装完成

![安装完成](http://git.oschina.net/uploads/images/2016/0107/182949_bcb9f70b_582.png "安装完成")